from django.urls import path
from .views import (
    GroupListView,
    GroupDetailView,
    GroupCreateView,
    GroupUpdateView,
    GroupDeleteView
)
from . import views

urlpatterns = [
    path('', GroupListView.as_view(), name='groups.all'),
    path('post/<int:pk>/',GroupDetailView.as_view(), name='group.detail'),
    path('post/new/', GroupCreateView.as_view(), name='group.create'),
    path('post/<int:pk>/update/', GroupUpdateView.as_view(), name='group.update'),
    path('post/<int:pk>/delete/', GroupDeleteView.as_view(), name='group.delete'),

]