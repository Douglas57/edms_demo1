from django.apps import AppConfig


class MultiAuthConfig(AppConfig):
    name = 'multi_auth'

    def ready(self):
        import multi_auth.signals
