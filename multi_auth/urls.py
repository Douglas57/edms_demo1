from django.urls import path
from django.contrib.auth import views as auth_views
from django.contrib.auth.models import Permission
from .views import (
    UserListView,
    UserDetailView,
    UserUpdateView,
    UserDeleteView,
    GroupListView,
    GroupUpdateView,

)
from . import views as multi_auth_view

urlpatterns = [
    path('', multi_auth_view.index, name='home'),
    # users
    path('users/', UserListView.as_view(), name='users.index'),
    path('users/create/', multi_auth_view.userCreate, name='users.create'),
    path('users/<int:pk>/', UserDetailView.as_view(), name='users.detail'),
    path('users/update/<int:pk>/', UserUpdateView.as_view(), name='user.update'),
    path('users/password_reset/', multi_auth_view.passwordResset, name='user.changepass'),

    path('user/delete/<int:pk>/', UserDeleteView.as_view(), name='user.delete'),
    # groups
    path('roles/', GroupListView.as_view(), name='groups.index'),
    path('roles/create/', multi_auth_view.addGroup, name='groups.create'),
    path('roles/update/<int:pk>/', GroupUpdateView.as_view(), name='groups.update'),
    #
    path('login/', multi_auth_view.login, name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='multi_auth/logout.html'), name='logout'),


]