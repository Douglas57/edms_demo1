from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth import logout
from django.contrib import messages
from .decorators import unauthenticated_user
from django.contrib.auth import authenticate, login as auth_login
from django.contrib.auth.models import Group, User, Permission
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin, PermissionRequiredMixin
from .forms import LoginForm, UserRegistrationForm,PasswordResetForm, GroupCreationForm
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)

@unauthenticated_user
def login(request):

    if request.method == 'POST':
        form = LoginForm(data=request.POST)
        if form.is_valid():
            uname = request.POST.get('username')
            pwd = request.POST.get('password')
            user = authenticate(request, username=uname, password=pwd)
            if user is not None:
                auth_login(request, user)
                if request.user.profile.first_Login:
                    return redirect('user.changepass')
                return redirect('users.index')

    else:
        form = LoginForm()
    return render(request, 'multi_auth/login.html', {

        'form': form,
    })


@login_required
def passwordResset(request):
    if request.method == 'POST':
        form = PasswordResetForm(user=request.user,data=request.POST)

        if form.is_valid():

            form.save()
            u = request.user
            u.profile.first_Login=False
            u.save()
            logout(request)
            return redirect('login')
    else:
        form = PasswordResetForm(user=request.user)
    return render(request, 'multi_auth/reset_password.html', {'form': form,})


@login_required
@permission_required('auth.add_group', raise_exception=True)
def addGroup(request):
    form = GroupCreationForm(request.POST)
    if form.is_valid():
        form.save()

        messages.success(request, f"Group Created successfully")
        return redirect('groups.index')

    else:
        form = GroupCreationForm()
    return render(request, 'multi_auth/groups/create.html', {'form': form})
    # if request.method == 'POST':
    #     name = request.POST.get('name')
    #     permissions = request.POST.getlist('permissions')
    #     group = Group(name=name)
    #     group.save()
    #
    #     for perm in permissions:
    #         group.permissions.add(perm)
    #
    #     return redirect('groups.index')
    # else:
    #     data = {
    #         'permissions': Permission.objects.all()
    #     }
    # return render(request, 'multi_auth/groups/create.html', data)


class UserListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    raise_exception = True
    permission_required = 'auth.view_user'
    model = User
    template_name = 'multi_auth/users/index.html'


class UserDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    # print(f"from detail"+s['ph'])
    permission_required = 'auth.view_user'
    raise_exception = True
    model = User
    template_name = 'multi_auth/users/detail.html'


class GroupListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    permission_required = 'auth.view_user'
    raise_exception = True
    model = Group
    template_name = 'multi_auth/groups/index.html'


class UserUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = 'auth.change_user'
    raise_exception = True
    model = User
    success_url = '/users/'

    fields = ['username', 'email', 'groups', 'is_superuser']
    template_name = 'multi_auth/users/edit.html'


class GroupUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = 'auth.change_group'
    raise_exception = True
    model = Group
    success_url = '/roles/'

    fields = ['name', 'permissions']
    template_name = 'multi_auth/groups/create.html'


class UserDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    permission_required = 'auth.delete_user'
    raise_exception = True
    model = User
    success_url = '/users/'
    template_name = ''


@login_required
def index(request):
    context = {
        'title': 'Admin Dashboard',
        'subtitle': 'Dashboard',
        'current_url': 'Dashboard'
    }

    # s=SessionStore()
    # s['ph']=request.session['phone']
    # s.create()
    # print(s['ph'])

    return render(request, 'multi_auth/index.html', context)


@login_required
@permission_required('auth.add_user', raise_exception=True)
def userCreate(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            id_no = request.POST.get('id_no')
            email = form.cleaned_data.get('email')
            username = form.cleaned_data.get('username')
            groups = form.cleaned_data.get('groups')
            is_superuser = request.POST.get('set_super_admin')

            if is_superuser == 'on':

                User.objects.create_superuser(username, email, id_no)
            else:
                user = User.objects.create_user(username, email, id_no)
                user.groups.set(groups)
            messages.success(request, f'User created successfully!')
            return redirect('users.index')

    else:
        form = UserRegistrationForm()
    return render(request, 'multi_auth/users/create.html', {
        'groups': Group.objects.all(),
        'form': form,
    })


@login_required
@permission_required('auth.add_group', raise_exception=True)
def groupCreate(request):
    return render(request, 'multi_auth/groups/create.html')


class GroupCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Group
    raise_exception = True

    permissions = Permission.objects.all()
    extra_context = permissions
    template_name = 'multi_auth/groups/create.html'
    fields = ['name', 'permission']
    success_url = 'home'
